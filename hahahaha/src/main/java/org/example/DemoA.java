package org.example;

public class DemoA {
    private String name;

    public String getName() {
        return name;
    }

    public DemoA setName(String name) {
        this.name = name+"After Change";
        return this;
    }
}
