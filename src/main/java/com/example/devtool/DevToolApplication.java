package com.example.devtool;

import org.example.DemoA;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class DevToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevToolApplication.class, args);
    }

@RestController
public static class HelloWorld {
    @GetMapping("test")
    public ResponseEntity<?> getTest() {
        DemoA demo = new DemoA();
        demo.setName("remote test");
        return ResponseEntity.ok(demo);
    }
}
}
